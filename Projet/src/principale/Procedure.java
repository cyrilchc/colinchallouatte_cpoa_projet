package principale;

import java.sql.*;

import dao.Connexion;
public class Procedure extends Connexion {
	
/*------------------------------P�riodicit�-----------------------------------*/
	
    public void insertPeriodicite(String vIdperiodicite, String vLibelle) {
        
    Connection laConnexion = creeConnexion();
    try {
        
        PreparedStatement requete = laConnexion.prepareStatement("INSERT  INTO  Periodicite(id_periodicite,libelle)\r\n" + "VALUES  (?,?)\r\n" + "SET erreur := 0;");
        requete.setString(1,vIdperiodicite);
        requete.setString(2,vLibelle);
        ResultSet res = requete.executeQuery();
        while (res.next()) 
            {
                /*demander une r�ponse pr�cise avec les coordonn�es dans le tableau*/
            }
                if(res != null) 
                {
                    res.close();
                }
                if(requete != null) 
                {
                    requete.close();
                }
                if(laConnexion != null)
                {
                laConnexion.close();
                }
         
        
			}
		catch (SQLException sqle) {
			System.out.println("Pb select" + sqle.getMessage());
		}
		
    }
    
    
/*----------------------------------------------------------------------------*/
    
    public void deletePeriodicite(String vIdperiodicite) {
        
    Connection laConnexion = creeConnexion();
    try {
        
        PreparedStatement requete = laConnexion.prepareStatement("delete from Periodicite where id_periodicite=?");
        requete.setString(1,vIdperiodicite);
        ResultSet res = requete.executeQuery();
        while (res.next()) 
            {
                /*demander une r�ponse pr�cise avec les coordonn�es dans le tableau*/
            }
                if(res != null) 
                {
                    res.close();
                }
                if(requete != null) 
                {
                    requete.close();
                }
                if(laConnexion != null)
                {
                laConnexion.close();
                }
         
        
			}
		catch (SQLException sqle) {
			System.out.println("Pb select" + sqle.getMessage());
		}
		
    }
    
    
/*----------------------------------------------------------------------------*/
    
    public void updatePeriodicite(String vIdperiodicite, String vLibelle) {
        
    Connection laConnexion = creeConnexion();
    try {
        
        PreparedStatement requete = laConnexion.prepareStatement("UPDATE  Periodicite\r\n" + "SET libelle  = ?,\r\n" +"WHERE  id_periodicite = ?;");
        requete.setString(1,vLibelle);
        requete.setString(2,vIdperiodicite);
        ResultSet res = requete.executeQuery();
        while (res.next()) 
            {
                /*demander une r�ponse pr�cise avec les coordonn�es dans le tableau*/
            }
                if(res != null) 
                {
                    res.close();
                }
                if(requete != null) 
                {
                    requete.close();
                }
                if(laConnexion != null)
                {
                laConnexion.close();
                }
         
        
			}
		catch (SQLException sqle) {
			System.out.println("Pb select" + sqle.getMessage());
		}
		
    }
    
/*-------------------------------Abonnement-----------------------------------*/ 
    
  //format date sql,java a chang�!!//
    public void insertAbonnement(String vIdclient, String vIdrevue, String vDatedebut, String vDatefin) {
        
        Connection laConnexion = creeConnexion();
        try {
            
            PreparedStatement requete = laConnexion.prepareStatement("INSERT  INTO  Abonnement(id_client,id_revue,date_debut,date_fin)\r\n" + "VALUES  (?,?,?,?)\r\n" + "SET erreur := 0;");
            requete.setString(1,vIdclient);
            requete.setString(2,vIdrevue);
            requete.setString(3,vDatedebut);
            requete.setString(4,vDatefin);
            ResultSet res = requete.executeQuery();
            while (res.next()) 
                {
                    /*demander une r�ponse pr�cise avec les coordonn�es dans le tableau*/
                }
                    if(res != null) 
                    {
                        res.close();
                    }
                    if(requete != null) 
                    {
                        requete.close();
                    }
                    if(laConnexion != null)
                    {
                    laConnexion.close();
                    }
             
            
    			}
    		catch (SQLException sqle) {
    			System.out.println("Pb select" + sqle.getMessage());
    		}
    		
        }  
    
/*----------------------------------------------------------------------------*/    
    
    public void deleteAbonnement(String vIdclient, String vIdrevue) {
        
        Connection laConnexion = creeConnexion();
        try {
            
            PreparedStatement requete = laConnexion.prepareStatement("delete  from  Abonnement where id_client=? and id_revue=?");
            
            requete.setString(1,vIdclient);
            requete.setString(2,vIdrevue);
            ResultSet res = requete.executeQuery();
            while (res.next()) 
                {
                    /*demander une r�ponse pr�cise avec les coordonn�es dans le tableau*/
                }
                    if(res != null) 
                    {
                        res.close();
                    }
                    if(requete != null) 
                    {
                        requete.close();
                    }
                    if(laConnexion != null)
                    {
                    laConnexion.close();
                    }
             
            
    			}
    		catch (SQLException sqle) {
    			System.out.println("Pb select" + sqle.getMessage());
    		}
    		
        }      
    
    
/*----------------------------------------------------------------------------*/    
    
    public void updateAbonnement(String vIdclient, String vIdrevue, String vDatedebut, String vDatefin) {
        
        Connection laConnexion = creeConnexion();
        try {
            
            PreparedStatement requete = laConnexion.prepareStatement("\"UPDATE  Abonnement\\r\\n\" + \"SET date_daebut  = ?,date_fin\\r\\n\" +\"where id_client=? and id_revue=?;");
            requete.setString(1,vDatedebut);
            requete.setString(2,vDatefin);
            requete.setString(3,vIdclient);
            requete.setString(4,vIdrevue);
            ResultSet res = requete.executeQuery();
            while (res.next()) 
                {
                    /*demander une r�ponse pr�cise avec les coordonn�es dans le tableau*/
                }
                    if(res != null) 
                    {
                        res.close();
                    }
                    if(requete != null) 
                    {
                        requete.close();
                    }
                    if(laConnexion != null)
                    {
                    laConnexion.close();
                    }
             
            
    			}
    		catch (SQLException sqle) {
    			System.out.println("Pb select" + sqle.getMessage());
    		}
    		
        } 
    
    
/*-------------------------------Client-------------------------------------*/  
    
    public void insertClient(String vIdclient, String vNom, String vPrenom, String vNorue, String vVoie, String vCodepostal, String vVille, String vPays) {
        
        Connection laConnexion = creeConnexion();
        try {
            
            PreparedStatement requete = laConnexion.prepareStatement("INSERT  INTO  Client(id_client,nom,prenom,no_rue,voie,code_postal,ville,pays)\r\n" + "VALUES  (?,?,?,?,?,?,?,?,)\r\n" + "SET erreur := 0;");
            requete.setString(1,vIdclient);
            requete.setString(2,vNom);
            requete.setString(3,vPrenom);
            requete.setString(4,vNorue);
            requete.setString(5,vVoie);
            requete.setString(6,vCodepostal);
            requete.setString(7,vVille);
            requete.setString(8,vPays);
            ResultSet res = requete.executeQuery();
            while (res.next()) 
                {
                    /*demander une r�ponse pr�cise avec les coordonn�es dans le tableau*/
                }
                    if(res != null) 
                    {
                        res.close();
                    }
                    if(requete != null) 
                    {
                        requete.close();
                    }
                    if(laConnexion != null)
                    {
                    laConnexion.close();
                    }
             
            
    			}
    		catch (SQLException sqle) {
    			System.out.println("Pb select" + sqle.getMessage());
    		}
    		
        }      
    
/*----------------------------------------------------------------------------*/   
    
    public void deleteClient(String vIdclient) {
        
        Connection laConnexion = creeConnexion();
        try {
            
            PreparedStatement requete = laConnexion.prepareStatement("\"delete  from  Client where id_client=?");
            requete.setString(1,vIdclient);
            ResultSet res = requete.executeQuery();
            while (res.next()) 
                {
                    /*demander une r�ponse pr�cise avec les coordonn�es dans le tableau*/
                }
                    if(res != null) 
                    {
                        res.close();
                    }
                    if(requete != null) 
                    {
                        requete.close();
                    }
                    if(laConnexion != null)
                    {
                    laConnexion.close();
                    }
             
            
    			}
    		catch (SQLException sqle) {
    			System.out.println("Pb select" + sqle.getMessage());
    		}
    		
        }    

/*----------------------------------------------------------------------------*/       
    
    public void updateClient(String vIdclient, String vNom, String vPrenom, String vNorue, String vVoie, String vCodepostal, String vVille, String vPays) {
        
        Connection laConnexion = creeConnexion();
        try {
            
            PreparedStatement requete = laConnexion.prepareStatement("UPDATE  Client\r\n" +"SET\r\n" +"nom  = ?,\r\n" +"nom = ?,\r\n" +"prenom = ?,\r\n" +"no_rue = ?,\r\n" +	"voie=v_cp,\r\n" +"codepostal=?,\r\n" +"ville=?,\r\n" +"pays=?,\r\n" +"WHERE  id_client = ?;\r\n" +"SET erreur := 0;");
            requete.setString(1,vNom);
            requete.setString(2,vPrenom);
            requete.setString(3,vNorue);
            requete.setString(4,vVoie);
            requete.setString(5,vCodepostal);
            requete.setString(6,vVille);
            requete.setString(7,vPays);
            requete.setString(8,vIdclient);
            ResultSet res = requete.executeQuery();
            while (res.next()) 
                {
                    /*demander une r�ponse pr�cise avec les coordonn�es dans le tableau*/
                }
                    if(res != null) 
                    {
                        res.close();
                    }
                    if(requete != null) 
                    {
                        requete.close();
                    }
                    if(laConnexion != null)
                    {
                    laConnexion.close();
                    }
             
            
    			}
    		catch (SQLException sqle) {
    			System.out.println("Pb select" + sqle.getMessage());
    		}
    		
        }      
       
/*-------------------------------Revue-------------------------------------*/      
    
    public void insertRevue(String vIdrevue, String vTitre, String vDescription, String vTarifnumero, String vVisuel, String vIdperiodicite) {
        
        Connection laConnexion = creeConnexion();
        try {
            
            PreparedStatement requete = laConnexion.prepareStatement("INSERT  INTO  Revue(id_revue,titre,description,tarif_numero,visuel,id_periodicite)\r\n" + "VALUES  (?,?,?,?,?,?)\r\n" + "SET erreur := 0;");
            requete.setString(1,vIdrevue);
            requete.setString(2,vTitre);
            requete.setString(3,vDescription);
            requete.setString(4,vTarifnumero);
            requete.setString(5,vVisuel);
            requete.setString(6,vIdperiodicite);
            ResultSet res = requete.executeQuery();
            while (res.next()) 
                {
                    /*demander une r�ponse pr�cise avec les coordonn�es dans le tableau*/
                }
                    if(res != null) 
                    {
                        res.close();
                    }
                    if(requete != null) 
                    {
                        requete.close();
                    }
                    if(laConnexion != null)
                    {
                    laConnexion.close();
                    }
             
            
    			}
    		catch (SQLException sqle) {
    			System.out.println("Pb select" + sqle.getMessage());
    		}
    		
        }      
    
/*----------------------------------------------------------------------------*/      
    
    public void deleteRevue(String vIdrevue) {
        
        Connection laConnexion = creeConnexion();
        try {
            
            PreparedStatement requete = laConnexion.prepareStatement("delete from Revue\r\n" + "WHERE id_revue=?;");
            requete.setString(1,vIdrevue);
            ResultSet res = requete.executeQuery();
            while (res.next()) 
                {
                    /*demander une r�ponse pr�cise avec les coordonn�es dans le tableau*/
                }
                    if(res != null) 
                    {
                        res.close();
                    }
                    if(requete != null) 
                    {
                        requete.close();
                    }
                    if(laConnexion != null)
                    {
                    laConnexion.close();
                    }
             
            
    			}
    		catch (SQLException sqle) {
    			System.out.println("Pb select" + sqle.getMessage());
    		}
    		
        }    
    
/*----------------------------------------------------------------------------*/      
    
    public void updateRevue(String vIdrevue, String vTitre, String vDescription, String vTarifnumero, String vVisuel, String vIdperiodicite) {
        
        Connection laConnexion = creeConnexion();
        try {
            
            PreparedStatement requete = laConnexion.prepareStatement("UPDATE Client\r\n" + "SET\r\n" +"nom  = ?,\r\n" +"titre = ?,\r\n" +"description = ?,\r\n" +"no_rue = ?,\r\n" +	"voie=v_cp,\r\n" +"codepostal=?,\r\n" +"tarif_numero=?,\r\n" +"visuel=?,\r\n" +"id_periodicite=?,\r\n"+"WHERE  id_revue = ?;\r\n" +"SET erreur := 0;"); 
            requete.setString(1,vTitre);
            requete.setString(2,vDescription);
            requete.setString(3,vTarifnumero);
            requete.setString(4,vVisuel);
            requete.setString(5,vIdperiodicite);
            requete.setString(6,vIdrevue);
            ResultSet res = requete.executeQuery();
            while (res.next()) 
                {
                    /*demander une r�ponse pr�cise avec les coordonn�es dans le tableau*/
                }
                    if(res != null) 
                    {
                        res.close();
                    }
                    if(requete != null) 
                    {
                        requete.close();
                    }
                    if(laConnexion != null)
                    {
                    laConnexion.close();
                    }
             
            
    			}
    		catch (SQLException sqle) {
    			System.out.println("Pb select" + sqle.getMessage());
    		}
    		
        }
}
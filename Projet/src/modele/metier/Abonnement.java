package modele.metier;

import java.sql.Date;

public class Abonnement {
	private int idClient;
	private int idRevue;
	private Date dateDebut;
	private Date dateFin;
	
	
	public Abonnement(Date dateDebut, Date dateFin) {
		super();
		this.idClient=-1;
		this.idRevue=-1;
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
	}
	

	
	public int getIdClient() {
		return idClient;
	}
	public void setIdClient(int idClient) {
		this.idClient = idClient;
	}
	public int getIdRevue() {
		return idRevue;
	}
	public void setIdRevue(int idRevue) {
		this.idRevue = idRevue;
	}
	public Date getDateDebut() {
		return dateDebut;
	}
	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}
	public Date getDateFin() {
		return dateFin;
	}
	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}
	@Override
	public String toString() {
		return "(" + (this.idClient>=0?this.idClient:"nouveau")+ (this.idRevue>=0?this.idRevue:"nouveau") + this.dateDebut+this.dateFin +") ";
	}
	

}
	
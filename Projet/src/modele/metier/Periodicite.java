package modele.metier;

public class Periodicite {
	private int idPeriodicite;
	private String libelle;
	
	public Periodicite(int idPeriodicite, String libelle) {
		super();
		this.idPeriodicite=-1;
		this.libelle = libelle;
	}
	
	public int getIdPeriodicite() {
		return idPeriodicite;
	}
	public void setIdPeriodicite(int idPeriodicite) {
		this.idPeriodicite = idPeriodicite;
	}
	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		if (libelle==null || libelle.trim().length()==0) {
			throw new IllegalArgumentException("libelle de la periodicité vide !");
			} 
			this.libelle =libelle;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Periodicite other = (Periodicite) obj;
		if (idPeriodicite != other.idPeriodicite)
			return false;
		if (libelle == null) {
			if (other.libelle != null)
				return false;
		} else if (!libelle.equals(other.libelle))
			return false;
		return true;
	}


	@Override
	public String toString() {
		return "(" + (this.idPeriodicite>=0?this.idPeriodicite:"nouveau") + ") " +this.libelle;
	}

	public void setId(int i) {
		// TODO Auto-generated method stub
		
	}

	public int getId() {
		// TODO Auto-generated method stub
		return 0;
	}


	

}

package modele.metier;

import java.time.LocalDate;

public class DatesAbonnement {

	private LocalDate dateDebut;
	private LocalDate dateFin;
	public DatesAbonnement(LocalDate dateDebut, LocalDate dateFin) {
		super();
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
	}
	public LocalDate getDateDebut() {
		return dateDebut;
	}
	public void setDateDebut(LocalDate dateDebut) {
		this.dateDebut = dateDebut;
	}
	public LocalDate getDateFin() {
		return dateFin;
	}
	public void setDateFin(LocalDate dateFin) {
		this.dateFin = dateFin;
	}
	@Override
	public String toString() {
		return "DatesAbonnement [dateDebut=" + dateDebut + ", dateFin=" + dateFin + "]";
	}
	
	
	
}

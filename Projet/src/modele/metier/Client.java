package modele.metier;

import java.util.HashMap;

public class Client {
	private int idClient;
	private String nom;
	private String prenom;
	private String noRue;
	private String voie;
	private String codePostal;
	private String ville;
	private String pays;
	private HashMap <Revue,DatesAbonnement> lesAbonnements;
	
	
	public Client(String nom, String prenom, String noRue, String voie, String codePostal,
			String ville, String pays, HashMap lesAbonnements) {
		super();
		this.idClient = -1;
		this.nom = nom;
		this.prenom = prenom;
		this.noRue = noRue;
		this.voie = voie;
		this.codePostal = codePostal;
		this.ville = ville;
		this.pays = pays;
		this.lesAbonnements = lesAbonnements;
	}
	
	
	public int getIdclient() {
		return idClient;
	}
	public void setIdClient(int idClient) {
		this.idClient = idClient;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		if (nom==null || nom.trim().length()==0) {
			throw new IllegalArgumentException("Nom Client vide !");
			}
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		if (prenom==null || prenom.trim().length()==0) {
			throw new IllegalArgumentException("Prenom de CLient vide !");
			}
		this.prenom = prenom;
	}
	public String getNoRue() {
		return noRue;
	}
	public void setNoRue(String noRue) {
			if (noRue==null || noRue.trim().length()==0) {
				throw new IllegalArgumentException("Norue de CLient vide !");
				}
			this.noRue = noRue;
	}
	public String getVoie() {
		return voie;
	}
	public void setVoie(String voie) {
		if (voie==null || voie.trim().length()==0) {
			throw new IllegalArgumentException("Voie de CLient vide !");
			}
		this.voie = voie;
	}
	public String getCodePostal() {
		return codePostal;
	}
	public void setCodePostal(String codePostal) {
		if (codePostal==null || codePostal.trim().length()==0) {
			throw new IllegalArgumentException("CodePostal de CLient vide !");
			}
		this.codePostal = codePostal;
	}
	public String getVille() {
		return ville;
	}
	public void setVille(String ville) {
		if (ville==null || ville.trim().length()==0) {
			throw new IllegalArgumentException("Ville de CLient vide !");
			}
		this.ville = ville;
	}
	public String getPays() {
		return pays;
	}
	public void setPays(String pays) {
		if (ville==null || ville.trim().length()==0) {
			throw new IllegalArgumentException("Pays de CLient vide !");
			}
		this.pays = pays;
	}


	
	public String toString() {
		return "(" + (this.idClient>=0?this.idClient:"nouveau") + ")" + this.nom + this.prenom + this.noRue + this.voie + this.codePostal + this.ville + this.pays;
				
	}
	
	
	
	
	
	

}

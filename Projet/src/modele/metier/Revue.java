package modele.metier;

public class Revue {
	private int idRevue;
	private String titre;
	private String description;
	private double tarifNumero;
	private String visuel;
	private int idPeriodicite;
	
	
	
	public Revue(String titre, String description, double tarifNumero, String visuel, int idPeriodicite) {
		super();
		this.idRevue = -1;
		this.titre = titre;
		this.description = description;
		this.tarifNumero = tarifNumero;
		this.visuel = visuel;
		this.idPeriodicite = idPeriodicite;
	}
	
	
	
	public int getIdRevue() {
		return idRevue;
	}
	
	public void setIdRevue(int idRevue) {
		this.idRevue = idRevue;
	}
	public String getTitre() {
		return titre;
	}
	public void setTitre(String titre) {
		if (titre==null || titre.trim().length()==0) {
			throw new IllegalArgumentException("titre de la revue vide !");
			} 
		this.titre =titre;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		if (description==null || description.trim().length()==0) {
			throw new IllegalArgumentException("description de la revue vide !");
			} 
		this.description =description;
	}
	public double getTarifNumero() {
		return tarifNumero;
	}
	public void setTarifNumero(double tarifNumero) {
		this.tarifNumero = tarifNumero;
	}
	public String getVisuel() {
		return visuel;
	}
	public void setVisuel(String visuel) {
		if (visuel==null || visuel.trim().length()==0) {
			throw new IllegalArgumentException("visuel de revue vide !");
			}
		this.visuel = visuel;
	}
	public int getIdPeriodicite() {
		return idPeriodicite;
	}
	public void setIdPeriodicite(int idPeriodicite) {
		this.idPeriodicite = idPeriodicite;
	}
	@Override
	public String toString() {
		return "(" + (this.idRevue>=0?this.idRevue:"nouveau") + ") " +this.titre + this.description + this.tarifNumero + this.visuel + this.idPeriodicite+")";
	}

}

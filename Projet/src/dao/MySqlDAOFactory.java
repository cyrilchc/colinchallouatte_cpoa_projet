package dao;

public class MySqlDAOFactory extends DAOFactory {

	@Override
	public ClientDAO getClientDAO() {
		return MySqlClientDAO.getInstance();
		}
	@Override
	public AbonnementDAO getAbonnementDAO() {
		return MySqlAbonnementDAO.getInstance();
		}


	@Override
	public RevueDAO getRevueDAO() {
		// TODO Auto-generated method stub
		return MySqlRevueDAO.getInstance();
	}

	@Override
	public PeriodiciteDAO getPeriodiciteDAO() {
		// TODO Auto-generated method stub
		return MySqlPeriodiciteDAO.getInstance();
	}
}

package dao;

public class XMLDAOFactory extends DAOFactory {
	@Override
	public ClientDAO getClientDAO() {
		return XMLClientDAO.getInstance();
		}
	@Override
	public AbonnementDAO getAbonnementDAO() {
		return XMLAbonnementDAO.getInstance();
		}


	@Override
	public RevueDAO getRevueDAO() {
		// TODO Auto-generated method stub
		return XMLRevueDAO.getInstance();
	}

	@Override
	public PeriodiciteDAO getPeriodiciteDAO() {
		// TODO Auto-generated method stub
		return XMLPeriodiciteDAO.getInstance();
	}

}

package dao;

import modele.metier.Revue;

public interface RevueDAO extends DAO<Revue> {

	public abstract <Etudiant>  void getByIDrevue();
	public abstract <Etudiant>  void getBytitre();
	public abstract <Etudiant>  void getByDescription();
	public abstract <Etudiant>  void getBytarifnumero();
	public abstract <Etudiant>  void getByvisuel();
	public abstract <Etudiant>  void getByIDperiodicite();


}

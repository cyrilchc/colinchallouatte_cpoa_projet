package dao;

import java.sql.*;
import java.util.List;

import modele.metier.Client;
import modele.metier.Periodicite;

public class MySqlPeriodiciteDAO implements PeriodiciteDAO {


	
	
	private static MySqlPeriodiciteDAO instance;
	private MySqlPeriodiciteDAO() {}
	
	public static PeriodiciteDAO getInstance()
	{
		if(instance==null) {
			instance = new MySqlPeriodiciteDAO();
		}
		return instance;
	}

	@Override
	public void update(Periodicite objet) {
		// TODO Auto-generated method stub
		Periodicite p =null;
		try {
		getInstance();
		Connexion laConnexion = new Connexion();
		PreparedStatement requete =(PreparedStatement) ((Connection) laConnexion).prepareStatement("UPDATE Periodicite SET libelle=? where id_periodicite = ?;");		
		requete.setString(1, ((Periodicite) p).getLibelle());
		requete.setInt(2, ((Periodicite) p).getId());
		int r = requete.executeUpdate();
	} catch (SQLException sqle) {
		System.out.println("Pb select" + sqle.getMessage());
		 } 
		
	}

	@Override
	public void create(Periodicite objet) {
		// TODO Auto-generated method stub
		Periodicite p =null;
		try {
		getInstance();
		Connexion laConnexion = new Connexion();
		PreparedStatement requete =(PreparedStatement) ((Connection) laConnexion).prepareStatement("INSERT INTO Periodicite(id_periodicite,libelle) values(NULL,?);");		
		requete.setString(1,((Periodicite) p).getLibelle());
		int r = requete.executeUpdate();
	} catch (SQLException sqle) {
		System.out.println("Pb select" + sqle.getMessage());
		 }
	}

	@Override
	public void delete(Periodicite objet) {
		// TODO Auto-generated method stub
		Periodicite p =null;
		try {
		getInstance();
		Connexion laConnexion = new Connexion();
		PreparedStatement requete =(PreparedStatement) ((Connection) laConnexion).prepareStatement("DELETE FROM Periodicite where id_periodicite = ?");		
		requete.setInt(1, ((Periodicite) p).getId());
		int r = requete.executeUpdate();
	} catch (SQLException sqle) {
		System.out.println("Pb select" + sqle.getMessage());
		 } 
	}

	@Override
	public List<Periodicite> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Periodicite getById(int id) {
		// TODO Auto-generated method stub
		return null;
	}


	
	


}

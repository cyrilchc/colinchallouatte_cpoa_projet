package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Connection;
import java.util.List;

import modele.metier.Client;

public class MySqlClientDAO implements ClientDAO {
	private static MySqlClientDAO instance;
	private MySqlClientDAO() {}
	
	public static ClientDAO getInstance()
	{
		if(instance==null) {
			instance = new MySqlClientDAO();
		}
		return instance;
	}
	@Override
	public void create(Object objet) {
		// TODO Auto-generated method stub
		try {
			Client c =null;
			getInstance();
			Connexion laConnexion = new Connexion();
			PreparedStatement requete =(PreparedStatement) ((Connection) laConnexion).prepareStatement("INSERT INTO Client(id_client,nom,prenom,no_rue,voie,code_postal,ville,pays) values(NULL,?,?,?,?,?,?,?);");		
			requete.setString(1, c.getNom());
			requete.setString(2, c.getPrenom());
			requete.setString(3,c.getVoie());
			requete.setString(4, c.getVoie());
			requete.setString(5, c.getCodePostal());
			requete.setString(6, c.getVille());
			requete.setString(7, c.getPays());
			int r = requete.executeUpdate();
		} catch (SQLException sqle) {
			System.out.println("Pb select" + sqle.getMessage());
			 } 
	}

	@Override
	public void update(Object objet) {
		// TODO Auto-generated method stub
		try {
			Client c =null;
			getInstance();
			Connexion laConnexion = new Connexion();
			PreparedStatement requete =(PreparedStatement) ((Connection) laConnexion).prepareStatement("UPDATE Client SET nom=?, prenom=?,no_rue=?,voie=?,code_postal=?,ville=?,pays=?  where id_client = ?;");		
			requete.setString(1, c.getNom());
			requete.setString(2, c.getPrenom());
			requete.setString(3, c.getVoie());
			requete.setString(4, c.getVoie());
			requete.setString(5, c.getCodePostal());
			requete.setString(6, c.getVille());
			requete.setString(7, c.getPays());
			requete.setInt(8, c.getIdclient());
			int r = requete.executeUpdate();
		} catch (SQLException sqle) {
			System.out.println("Pb select" + sqle.getMessage());
			 }
	}

	@Override
	public void delete(Object objet) {
		// TODO Auto-generated method stub
		Client c =null;
		try {
			getInstance();
			Connexion laConnexion = new Connexion();
			PreparedStatement requete =(PreparedStatement) ((Connection) laConnexion).prepareStatement("DELETE FROM Client where id_client = ?");		
			requete.setInt(1, c.getIdclient());
			int r = requete.executeUpdate();
		} catch (SQLException sqle) {
			System.out.println("Pb select" + sqle.getMessage());
			 } 
	}

	@Override
	public void getByIDclient() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public List<Client> findAll() {
		// TODO Auto-generated method stub
		return null;
	}
	
	


	@Override
	public Object getById(int id) {
		// TODO Auto-generated method stub
		Client c =null;
		try {
		getInstance();
		Connexion laConnexion = new Connexion();
		PreparedStatement requete =(PreparedStatement) ((Connection) laConnexion).prepareStatement("select * from Client where id_client=?");
		requete.setInt(1, id);
		ResultSet res = requete.executeQuery();
		while (res.next()){ for (int i=1;i<=8;i++){ System.out.println(res.getString(i));}
		}
	}catch (SQLException sqle) {
		System.out.println("Pb select" + sqle.getMessage());
	}
	return c;	
	}


	
	
	


	

}

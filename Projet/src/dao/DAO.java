package dao;

import java.util.List;

import modele.metier.Periodicite;

public interface DAO <T> {
	public abstract T getById(int id);
	public abstract void create(T objet);
	public abstract void update(T objet);
	public abstract void delete(T objet);
	public List<T> findAll();


}

package dao;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
public class Connexion {
	private Properties accesBdd;
	public Connection creeConnexion() {
		String url ="jdbc:mysql://accesBdd.getProperty(\"adresseIp\"):accesBdd.getProperty(\"port\")/accesBdd.getProperty(\"bdd\")";
		String bdd = accesBdd.getProperty("bdd");
		String port = accesBdd.getProperty("port");
		String login = accesBdd.getProperty("login");
		String pwd =accesBdd.getProperty("pass");
		String adressIp=accesBdd.getProperty("adresse_ip");
		Connection maConnexion = null;
		try {
			
			maConnexion = DriverManager.getConnection(url,login,pwd);//les liens properties � ajouter?
			System.out.println("yeah");
		}catch (SQLException sqle) {
			System.out.println(sqle.getMessage());
		}
		return maConnexion;
	}

	private void LireFichier() {
		
		accesBdd = new Properties();
		File fBdd = new File("config/bdd.properties");
		
		try {
			FileInputStream source = new FileInputStream(fBdd);
			accesBdd.loadFromXML(source);
		}
		
		catch (IOException ioe) {
			ioe.printStackTrace();
		}
		
		System.out.println(accesBdd.getProperty("login"));
		System.out.println(accesBdd.getProperty("pass"));
		System.out.println(accesBdd.getProperty("bdd"));
		System.out.println(accesBdd.getProperty("port"));
		System.out.println(accesBdd.getProperty("adresse_ip"));
		
	}
	
	private static Connexion instance;
	Connexion() {LireFichier();}
	
	public static Connexion getInstance() {
		if(instance==null) {
			instance = new Connexion();
		}
		return instance;
	}
	
}

package dao.listememoire;

import java.util.ArrayList;
import java.util.List;

import dao.RevueDAO;
import modele.metier.Client;
import modele.metier.Revue;

public class ListeMemoireRevueDAO implements RevueDAO{
	
	
	private static ListeMemoireRevueDAO instance;

	private List<Revue> donnees;


	public static ListeMemoireRevueDAO getInstance() {

		if (instance == null) {
			instance = new ListeMemoireRevueDAO();
		}

		return instance;
	}

	private ListeMemoireRevueDAO() {

		this.donnees = new ArrayList<Revue>();

		this.donnees.add(new Revue("Micro", "Informatique",20.0,"visu1",1));
		this.donnees.add(new Revue("Science et vie", "Science",15.0,"visu2",2));
	}

	@Override
	public Revue getById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void create(Revue objet) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(Revue objet) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Revue objet) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Revue> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <Etudiant> void getByIDrevue() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public <Etudiant> void getBytitre() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public <Etudiant> void getByDescription() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public <Etudiant> void getBytarifnumero() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public <Etudiant> void getByvisuel() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public <Etudiant> void getByIDperiodicite() {
		// TODO Auto-generated method stub
		
	}




}
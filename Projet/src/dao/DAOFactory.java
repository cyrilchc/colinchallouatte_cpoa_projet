package dao;

public abstract class DAOFactory {
public static DAOFactory getDAOFactory(Persistance cible) {
DAOFactory daoF = null;
switch (cible) {
case MYSQL:
daoF = new MySqlDAOFactory();
break;
case XML:
daoF = new XMLDAOFactory();
break;
}
return daoF;
}
public abstract AbonnementDAO getAbonnementDAO();
public abstract ClientDAO getClientDAO();
public abstract RevueDAO getRevueDAO();
public abstract PeriodiciteDAO getPeriodiciteDAO();

}
